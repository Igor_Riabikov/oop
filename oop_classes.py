from math import sqrt


class Shape:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Point(Shape):
    pass


class Circle(Shape):
    def __init__(self, x, y, radius):
        super().__init__(x, y)
        self.radius = radius

    def is_point_in_circle(self, point):
        gip = sqrt((self.x-point.x)**2+(self.y-point.y)**2)
        if gip <= self.radius:
            print('Point in the circle\n')
            return True
        else:
            print('Point out of the circle\n')
            return False

    def __contains__(self, point):
        if not isinstance(point, Point):
            raise ValueError("It's not Point")
        return self.is_point_in_circle(point)


point = Point(55, 16)
c1 = Circle(20, 20, 15)
if point in c1:
    pass


# Robots


class Robot:
    def __init__(self, model):
        self.model = model
        self.source_of_power = "Battery"
        self.movement = True

    def about(self):
        print(f"Hello. I'm - {self.model}")


class SpotMini(Robot):
    def __init__(self, model, color):
        super().__init__(model)
        self.color = color

    def color_info(self):
        print(f"WOW, I'm {self.color} today!\n")


class Atlas(Robot):
    def __init__(self, model, speed):
        super().__init__(model)
        self.speed = speed

    def speed_info(self):
        print(f"My speed is {self.speed} m/s.\n")


class Handle(Robot):
    def __init__(self, model, capacity):
        super().__init__(model)
        self.capacity = capacity

    def capacity_info(self):
        print(f"I'm very strong. My capacity is {self.capacity} kg!\n")


s1 = SpotMini("SPOT", "Yellow")
s1.about()
s1.color_info()

a1 = Atlas("Atlas", 4)
a1.about()
a1.speed_info()

h1 = Handle("Handle", 15)
h1.about()
h1.capacity_info()
